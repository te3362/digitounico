package com.teste.digitounico.controller.digitounico;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.teste.digitounico.DigitounicoApplication;
import com.teste.digitounico.controller.DigitoUnicoController;
import com.teste.digitounico.dto.DigitoUnicoParamDto;
import com.teste.digitounico.dto.DigitoUnicoResponseDto;
import com.teste.digitounico.service.DigitoUnicoService;

@SpringBootTest(classes = DigitounicoApplication.class)
public class DigitoUnicoControllerTest {
	
	private static final String USER_HASH_ID = "STRINGHASHID";
	
	@Autowired
	private DigitoUnicoController controller;
	
	@MockBean
	private DigitoUnicoService service;
	
	@Test
	void store() {
		Mockito.when(service.store(Mockito.mock(DigitoUnicoParamDto.class))).thenReturn(Mockito.mock(DigitoUnicoResponseDto.class));
		var response = controller.store(Mockito.mock(DigitoUnicoParamDto.class));
		Assertions.assertEquals(200, response.getStatusCodeValue());
	}
	
	@Test
	void show() {
		Mockito.when(service.showByUsuario(USER_HASH_ID)).thenReturn(new ArrayList<>());
		var response = controller.show(USER_HASH_ID);
		Assertions.assertEquals(200, response.getStatusCodeValue());
	}
}
