package com.teste.digitounico.controller.usuario;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.teste.digitounico.DigitounicoApplication;
import com.teste.digitounico.controller.UsuarioController;
import com.teste.digitounico.dto.UsuarioDto;
import com.teste.digitounico.service.UsuarioService;

@SpringBootTest(classes = DigitounicoApplication.class)
public class UsuarioControllerTest {
	
	private static final String USER_HASH_ID = "STRINGHASHID";

	@Autowired
	private UsuarioController controller;
	
	@MockBean
	private UsuarioService service;
	
	@Test
	void store() {		
		Mockito.when(service.store(Mockito.mock(UsuarioDto.class))).thenReturn(USER_HASH_ID);
		var response = controller.store(Mockito.mock(UsuarioDto.class));
		Assertions.assertEquals(200, response.getStatusCodeValue());
	}
	
	@Test
	void update() {
		Mockito.doNothing().when(service).update(Mockito.mock(UsuarioDto.class), USER_HASH_ID);
		var response = controller.update(Mockito.mock(UsuarioDto.class), USER_HASH_ID);
		Assertions.assertEquals(200, response.getStatusCodeValue());
	}
}
