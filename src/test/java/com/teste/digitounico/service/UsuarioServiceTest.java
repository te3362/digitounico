package com.teste.digitounico.service;


import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Optional;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.teste.digitounico.DigitounicoApplication;
import com.teste.digitounico.dto.UsuarioDto;
import com.teste.digitounico.model.Usuario;
import com.teste.digitounico.repository.UsuarioRepository;

@SpringBootTest(classes = DigitounicoApplication.class)
public class UsuarioServiceTest {
	
	@Autowired
	private UsuarioService service;
	
	@MockBean
	private UsuarioRepository repository;
	
	private Usuario usuario = new Usuario();
	
	@Test
	void store() {
		var response = service.store(Mockito.mock(UsuarioDto.class));
		Assertions.assertNotNull(response);
	}
	
	@Test
	void update() {
		Mockito.when(repository.findByIdHash(null)).thenReturn(Optional.of(usuario));
		service.update(Mockito.mock(UsuarioDto.class), null);
		Mockito.verify(repository, Mockito.times(1)).saveAndFlush(Mockito.any(Usuario.class));
	}
	
	@Test
	void delete() {
		Mockito.when(repository.findByIdHash(null)).thenReturn(Optional.of(usuario));
		service.delete(null);
		Mockito.verify(repository, Mockito.times(1)).delete(Mockito.any(Usuario.class));
	}

	@Test
	void show() {
		Mockito.when(repository.findByIdHash(null)).thenReturn(Optional.of(usuario));
		var response = service.show(null);
		Assertions.assertEquals(UsuarioDto.class, response.getClass());
	}
	
	@Test
	void encript() throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		UsuarioDto usuarioMock = new UsuarioDto();
		usuarioMock.setEmail("teste@teste");
		usuario.setEmail("teste@teste");
		usuario.setNome("Tester");

		KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
        keygen.initialize(2048, new SecureRandom());
        KeyPair keyPair = keygen.generateKeyPair();
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        byte[] publicKeyByte = Base64.getEncoder().encode(publicKey.getEncoded());
        String publicKeyString = new String(publicKeyByte);
        
        Mockito.when(repository.findByIdHash(null)).thenReturn(Optional.of(usuario));
		var response = service.encript(null, publicKeyString);
		String emailDescriptografado = descriptografarTexto(response.getEmail(), privateKey);
		Assertions.assertEquals(usuarioMock.getEmail(), emailDescriptografado);
	}

	private String descriptografarTexto(String email, PrivateKey privateKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        var texto = email.getBytes();
        var base64 = Base64.getDecoder();
        var decodeValue = base64.decode(texto);
        var cipherFinal = cipher.doFinal(decodeValue);
        var response = new String(cipherFinal);
        return response;
	}
	
}
