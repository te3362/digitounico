package com.teste.digitounico.service;

import javax.validation.Valid;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.teste.digitounico.DigitounicoApplication;
import com.teste.digitounico.dto.DigitoUnicoParamDto;
import com.teste.digitounico.repository.DigitoUnicoRepository;

@SpringBootTest(classes = DigitounicoApplication.class)
public class DigitoUnicoServiceTest {

	@Autowired
	private DigitoUnicoService service;

	@MockBean
	private DigitoUnicoRepository repository;
	
	@MockBean
	private UsuarioService usuarioService;
	
	@Test
	void store() {
		var response = service.store(digitoUnicoParamDtoMock());
		Assertions.assertEquals(8, response.getDigitoUnico());
	}

	private @Valid DigitoUnicoParamDto digitoUnicoParamDtoMock() {
		DigitoUnicoParamDto dto = new DigitoUnicoParamDto();
		dto.setNumeral("9875");
		dto.setRepeticoes(4);
		return dto;
	}
}
