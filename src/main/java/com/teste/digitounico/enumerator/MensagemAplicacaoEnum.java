package com.teste.digitounico.enumerator;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public enum MensagemAplicacaoEnum {
	X0_NAO_ENCONTRADO("x0.naoEncontrado"),
	ERRO_GERAR_HASH("erroGerarHash"),
	FORMATO_INVALIDO("erroFormatoNumeral"),
	X0_MENOR_QUE_X1("x0.menor.que.x1");

	MensagemAplicacaoEnum(String valor) {
		this.valor = valor;
	}

	private String valor;
	
	public String getValor() {
        return valor;
    }

    public String trataMensagem() {
        ResourceBundle bundle = ResourceBundle.getBundle("messages", Locale.getDefault());
        return bundle.getString(this.getValor());
    }
    
    public String trataMensagem(Object... args) {
        ResourceBundle bundle = ResourceBundle.getBundle("messages", Locale.getDefault());
        String mensagem = bundle.getString(this.getValor());
        return MessageFormat.format(mensagem, args);
    }

}
