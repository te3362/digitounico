package com.teste.digitounico.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teste.digitounico.model.DigitoUnico;
import com.teste.digitounico.model.Usuario;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {

	List<DigitoUnico> findAllByUsuario(Usuario usuario);

}
