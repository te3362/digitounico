package com.teste.digitounico.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DigitoUnicoResponseDto {

	private String numeral;

	private Integer repeticoes;
	
	private Long digitoUnico;
}
