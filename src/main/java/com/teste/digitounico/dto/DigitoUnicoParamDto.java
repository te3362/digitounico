package com.teste.digitounico.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DigitoUnicoParamDto {
	@NotBlank
	@NotNull
	private String numeral;

	@Max(100000)
	@Min(1)
	@NotNull
	private Integer repeticoes;

	private String usuarioHash;
}
