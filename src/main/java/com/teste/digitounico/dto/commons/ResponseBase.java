package com.teste.digitounico.dto.commons;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseBase<T> {
	@Builder.Default
	private boolean success = true;
	private int status;
	@Builder.Default
	private String message = "SUCCESS OPERATION";
	private String idPublico;
	private T data;
}
