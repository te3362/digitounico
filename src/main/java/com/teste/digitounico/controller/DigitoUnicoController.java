package com.teste.digitounico.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teste.digitounico.dto.DigitoUnicoParamDto;
import com.teste.digitounico.dto.DigitoUnicoResponseDto;
import com.teste.digitounico.dto.commons.ResponseBase;
import com.teste.digitounico.service.DigitoUnicoService;

@RestController
@RequestMapping("/digito")
public class DigitoUnicoController {
	
	@Autowired
	private DigitoUnicoService service;

	@PostMapping()
	public ResponseEntity<ResponseBase<Object>> store(@Valid @RequestBody DigitoUnicoParamDto dto){
		DigitoUnicoResponseDto responseData = service.store(dto);
		new ResponseBase<DigitoUnicoResponseDto>();
		var response = ResponseBase.builder().status(HttpStatus.CREATED.value()).data(responseData).build();
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<ResponseBase<Object>> show(@PathVariable String id){
		List<DigitoUnicoResponseDto> digitosUsuario = service.showByUsuario(id);
		new ResponseBase<DigitoUnicoResponseDto>();
		var response = ResponseBase.builder().status(HttpStatus.OK.value()).data(digitosUsuario).idPublico(id).build();
		return ResponseEntity.ok(response);
	}
}
