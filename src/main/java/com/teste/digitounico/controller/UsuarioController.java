package com.teste.digitounico.controller;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.teste.digitounico.dto.UsuarioDto;
import com.teste.digitounico.dto.commons.ResponseBase;
import com.teste.digitounico.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService service;

	@PostMapping()
	public ResponseEntity<ResponseBase<Object>> store(@Valid @RequestBody UsuarioDto dto){
		String idHash = service.store(dto);
		new ResponseBase<UsuarioDto>();
		var response = ResponseBase.builder().status(HttpStatus.CREATED.value()).data(dto).idPublico(idHash).build();
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<ResponseBase<Object>> update(@Valid @RequestBody UsuarioDto dto, @PathVariable String id){
		service.update(dto, id);
		new ResponseBase<UsuarioDto>();
		var response = ResponseBase.builder().status(HttpStatus.OK.value()).data(dto).idPublico(id).build();
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable String id){
		service.delete(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("{id}")
	public ResponseEntity<ResponseBase<Object>> show(@PathVariable String id){
		UsuarioDto usuario = service.show(id);
		new ResponseBase<UsuarioDto>();
		var response = ResponseBase.builder().status(HttpStatus.OK.value()).data(usuario).idPublico(id).build();
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("encript/{id}")
	public ResponseEntity<ResponseBase<Object>> encript(@PathVariable String id, @RequestParam String chavePublica) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		UsuarioDto usuario = service.encript(id, chavePublica);
		new ResponseBase<UsuarioDto>();
		var response = ResponseBase.builder().status(HttpStatus.OK.value()).data(usuario).idPublico(id).data(usuario).build();
		return ResponseEntity.ok(response);
	}
	
}
