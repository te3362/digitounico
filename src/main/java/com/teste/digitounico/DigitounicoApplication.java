package com.teste.digitounico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitounicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigitounicoApplication.class, args);
	}

}
