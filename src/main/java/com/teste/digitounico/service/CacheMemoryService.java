package com.teste.digitounico.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

import com.teste.digitounico.model.DigitoUnico;

@Service
@Scope("singleton")
public class CacheMemoryService {
    Map<String, DigitoUnico> cacheMemory = new LinkedHashMap<>();

	public boolean containsKey(String key) {
		return cacheMemory.containsKey(key);
	}

	public DigitoUnico get(String key) {
		return cacheMemory.get(key);
	}

	public void put(String key, DigitoUnico digitoCalculado) {
		if(cacheMemory.size() == 10) {
			cacheMemory.remove(getFirstElement());
		}
		cacheMemory.put(key, digitoCalculado);
	}

	private String getFirstElement() {
		Set<String> keyValues = cacheMemory.keySet();
		return keyValues.toArray()[0].toString();
	}
	

}
