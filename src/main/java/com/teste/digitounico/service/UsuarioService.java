package com.teste.digitounico.service;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Optional;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.digitounico.dto.UsuarioDto;
import com.teste.digitounico.enumerator.MensagemAplicacaoEnum;
import com.teste.digitounico.exception.NegocioException;
import com.teste.digitounico.model.Usuario;
import com.teste.digitounico.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;
	
	public String store(UsuarioDto dto) {
		Usuario usuario = fromParamDtoToEntity(dto);
		usuario.setIdHash(gerarIdHash(dto.getEmail()));
		repository.save(usuario);
		return usuario.getIdHash();
	}

	protected String gerarIdHash(String email) {
		String chave = LocalDateTime.now().toString() + email;
		
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new NegocioException(MensagemAplicacaoEnum.ERRO_GERAR_HASH.trataMensagem());
		}
	    md.update(chave.getBytes());
	    byte[] digest = md.digest();
	    String myHash = DatatypeConverter
	      .printHexBinary(digest).toUpperCase();
		
		return myHash;
	}

	private Usuario fromParamDtoToEntity(UsuarioDto dto) {
		return new ModelMapper().map(dto, Usuario.class);
	}

	public void update(@Valid UsuarioDto dto, String idHash) {
		Optional<Usuario> usuario = findByIdHash(idHash);
		Usuario usuarioAtualizado = fromParamDtoToEntity(dto);
		usuarioAtualizado.setId(usuario.get().getId());
		usuarioAtualizado.setIdHash(idHash);
		repository.saveAndFlush(usuarioAtualizado);
		
	}

	private void verificarUsuarioPresente(Optional<Usuario> usuario) {
		if(!usuario.isPresent()) {
			throw new NegocioException(MensagemAplicacaoEnum.X0_NAO_ENCONTRADO.trataMensagem("Usuario"));
		}
	}

	public void delete(String id) {
		Optional<Usuario> usuario = findByIdHash(id);
		repository.delete(usuario.get());
	}

	public UsuarioDto show(String id) {
		Optional<Usuario> usuario = findByIdHash(id);
		return fromEntityToDto(usuario.get());
	}

	public Optional<Usuario> findByIdHash(String idHash) {
		Optional<Usuario> response = repository.findByIdHash(idHash);
		verificarUsuarioPresente(response);
		return response;
	}

	private UsuarioDto fromEntityToDto(Usuario usuario) {
		return new ModelMapper().map(usuario, UsuarioDto.class);
	}

	public UsuarioDto encript(String id, String chaveStr) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		Usuario usuario = findByIdHash(id).get();
		
		PublicKey pubKey = recuperarChavePublica(chaveStr);
		
		usuario.setEmail(criptografrarTexto(usuario.getEmail(), pubKey));
		usuario.setNome(criptografrarTexto(usuario.getNome(), pubKey));

		return fromEntityToDto(usuario);
	}

	private PublicKey recuperarChavePublica(String chaveStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] publicBytes = Base64.getDecoder().decode(chaveStr); 
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(keySpec);
	}

	private String criptografrarTexto(String texto, PublicKey chavePublica) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, chavePublica);
		return new String(Base64.getEncoder().encode(cipher.doFinal(texto.getBytes())));
	}

}
