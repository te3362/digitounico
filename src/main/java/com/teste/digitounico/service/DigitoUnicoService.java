package com.teste.digitounico.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.digitounico.dto.DigitoUnicoParamDto;
import com.teste.digitounico.dto.DigitoUnicoResponseDto;
import com.teste.digitounico.enumerator.MensagemAplicacaoEnum;
import com.teste.digitounico.exception.NegocioException;
import com.teste.digitounico.model.DigitoUnico;
import com.teste.digitounico.model.Usuario;
import com.teste.digitounico.repository.DigitoUnicoRepository;

@Service
public class DigitoUnicoService {
	private static final Long MIN_VALUE_NUMERAL = 1L;
	
	@Autowired
	private DigitoUnicoRepository repository;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private CacheMemoryService cacheMemoryService;

	public DigitoUnicoResponseDto store(@Valid DigitoUnicoParamDto dto) {
		validarNumeral(dto.getNumeral());
		DigitoUnico digitoUnico = recuperarDigitoUnico(dto);
		Optional<Usuario> usuarioVinculado = dto.getUsuarioHash() != null && !dto.getUsuarioHash().isBlank() ? usuarioService.findByIdHash(dto.getUsuarioHash()) : Optional.empty();
		if(usuarioVinculado.isPresent()) {
			digitoUnico.setUsuario(usuarioVinculado.get());
			repository.save(digitoUnico);
		}
		return fromEntityToDto(digitoUnico);
	}

	private DigitoUnico recuperarDigitoUnico(@Valid DigitoUnicoParamDto dto) {
		String key = dto.getNumeral() + dto.getRepeticoes().toString();
		if(cacheMemoryService.containsKey(key)) {
			return cacheMemoryService.get(key);
		}
		Long digitoUnico = calcularDigitoUnico(dto.getNumeral().repeat(dto.getRepeticoes())); 
		DigitoUnico digitoCalculado = fromDtoToEntity(dto);
		digitoCalculado.setDigitoUnico(digitoUnico);
		cacheMemoryService.put(key, digitoCalculado);
		return digitoCalculado;
	}

	private DigitoUnico fromDtoToEntity(DigitoUnicoParamDto dto) {
		return new ModelMapper().map(dto, DigitoUnico.class);
	}

	private Long calcularDigitoUnico(String str) {
		
		if(str.length() == 1) {
			return Long.parseLong(str); 
		}

		Long valorTotal = 0L;
		while(str.length() != 1) {
			valorTotal = 0L;
			
			str = str.replace("0", "");
			String[] arr = str.split("");
			for(String atual : arr) {
				valorTotal = valorTotal + Long.parseLong(atual);
			}
			str = valorTotal.toString();
		}
		return valorTotal;
	}

	private void validarNumeral(String strNum) {
	    Long response; 
	    try {
	        response = Long.parseLong(strNum);
	    } catch (NumberFormatException nfe) {
	    	throw new NegocioException(MensagemAplicacaoEnum.FORMATO_INVALIDO.trataMensagem());
	    }
	    if(response < MIN_VALUE_NUMERAL) {
	    	throw new NegocioException(MensagemAplicacaoEnum.X0_MENOR_QUE_X1.trataMensagem("numeral", MIN_VALUE_NUMERAL.toString()));
	    }
	}

	public List<DigitoUnicoResponseDto> showByUsuario(String idHash) {
		Optional<Usuario> usuario = usuarioService.findByIdHash(idHash);
		List<DigitoUnico> digitosUsuario = repository.findAllByUsuario(usuario.get());
		return digitosUsuario.stream()
				.map(user -> fromEntityToDto(user)).collect(Collectors.toList());
	}
	
	private DigitoUnicoResponseDto fromEntityToDto(DigitoUnico digitoUnico) {
		return new ModelMapper().map(digitoUnico, DigitoUnicoResponseDto.class);
	}
	
	
}
