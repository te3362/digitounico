# Digito Unico - Cálculo de dígito único e gerenciamento de usuários
> REST API construída em Java com Spring Boot para efetuar cálculo de dígito único e gerenciamento de usuários.

### Recursos e funcionalidades

- [x] Cadastro de usuário
- [x] Cálculo de dígito único
- [x] Vinculação de cálculos a usuário
- [x] Criptografia de dados

## 💻 Pré-requisitos
> JDK 11
> IDE - Eclipse | Spring Tools Suite | Intelij
> Maven

### Built With

### Stacks presentes neste projeto

* [SpringBoot](https://spring.io/projects/spring-boot)
* [Junit](https://junit.org/junit5/)
* [Java 11](https://www.java.com/pt-BR/)
* [Swagger](https://swagger.io)
* [Criptografia por chaves assimétricas(RSA)](https://docs.oracle.com/)

## 🚀 Executando o projeto

Clone o repositório em um diretório de sua máquina
Abra o projeto em sua IDE de preferência
Execute o comando MVN CLEAN INSTALL
Execute a classe `DigitounicoApplication`

-Pronto: Aplicação ta on :muscle:

Em ambiente local você pode acessar a documentação do swagger e fazer as requisições para a API.
* [Documentação swagger com endpoints](http://localhost:8080/digitounico/swagger-ui/index.html#/)
* [Documentação](http://localhost:8080/digitounico/v3/api-docs)


## 📘 Lista de endpoints
  
usuario-controller:
  
  * GET/digitounico/usuario/{idPublico}  
  * PUT/digitounico/usuario/{idPublico}
  * DELETE/digitounico/usuario/{idPublico}
  * PUT/digitounico/encript/usuario/{idPublico}
  * POST/digitounicousuario

digito-unico-controller:
  
  * POST/digitounico/digito
  * GET/digitounico/digito/{id}

<p align="right">(<a href="#top">back to top</a>)</p>
